package com.shpp.pzobenko;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class Level2Test {
    @Test
    void defineTheType() {
        String nameOfTypePropSystem = "EMPTY";
        String expected = "int" ;
        assertEquals(expected,Level2.defineTheType(nameOfTypePropSystem));
    }

    @Test
    void findTypeFromProp() {
        String nameOfType = "EMPTY";
        assertFalse(Level2.findTypeFromProp(nameOfType));
        nameOfType = "InT";
        assertTrue(Level2.findTypeFromProp(nameOfType));
        nameOfType = "DouBle";
        assertTrue(Level2.findTypeFromProp(nameOfType));
        nameOfType = "FlOAT";
        assertTrue(Level2.findTypeFromProp(nameOfType));
        nameOfType = "LONG";
        assertTrue(Level2.findTypeFromProp(nameOfType));
        nameOfType = "Byte";
        assertTrue(Level2.findTypeFromProp(nameOfType));
        nameOfType = "Haha";
        assertFalse(Level2.findTypeFromProp(nameOfType));
    }

    @Test
    void multiplicationTable() {
        String expected = "\"1.0\" \"2.9\" \"4.8\" \n\"2.9\" \"8.41\" \"13.92\" \n\"4.8\" \"13.92\" \"23.04\"";
        double min = 1;
        double max = 5;
        double increment = 1.9;
        String type = "float";
        assertTrue(Level2.multiplicationTable(min, max, increment, type).contains(expected));
        expected = "\"1\" \"2\" \"3\" \"4\" \"5\" \"6\" \"7\" \"8\" \"9\" \"10\" \n\"2\" \"4\" \"6\" \"8\" \"10\" " +
                "\"12\" \"14\" \"16\" \"18\" \"20\" \n\"3\" \"6\" \"9\" \"12\" \"15\" \"18\" \"21\" \"24\" \"27\" " +
                "\"30\" \n\"4\" \"8\" \"12\" \"16\" \"20\" \"24\" \"28\" \"32\" \"36\" \"40\" \n\"5\" \"10\" \"15\" " +
                "\"20\" \"25\" \"30\" \"35\" \"40\" \"45\" \"50\" \n\"6\" \"12\" \"18\" \"24\" \"30\" \"36\" \"42\" " +
                "\"48\" \"54\" \"60\" \n\"7\" \"14\" \"21\" \"28\" \"35\" \"42\" \"49\" \"56\" \"63\" \"70\" \n\"8\" " +
                "\"16\" \"24\" \"32\" \"40\" \"48\" \"56\" \"64\" \"72\" \"80\" \n\"9\" \"18\" \"27\" \"36\" \"45\" " +
                "\"54\" \"63\" \"72\" \"81\" \"90\" \n\"10\" \"20\" \"30\" \"40\" \"50\" \"60\" \"70\" \"80\" \"90\" " +
                "\"100\"";
        max = 10;
        increment = 1;
        type = "short";
        assertTrue(Level2.multiplicationTable(min, max, increment, type).contains(expected));
    }

    @Test
    void setMinSizeOfType() {
        String type = "byte";
        Double expected = (double) Byte.MIN_VALUE;
        assertEquals(expected,Level2.setMinSizeOfType(type));
        type = "int";
        expected = (double) Integer.MIN_VALUE;
        assertEquals(expected,Level2.setMinSizeOfType(type));
        type = "double";
        expected = Double.MIN_VALUE;
        assertEquals(expected,Level2.setMinSizeOfType(type));
        type = "long";
        expected = (double) Long.MIN_VALUE;
        assertEquals(expected,Level2.setMinSizeOfType(type));
        long l = Long.MIN_VALUE;
        double dl = (double) Long.MIN_VALUE;
        assertEquals(l,dl);
        int i = Integer.MIN_VALUE;
        double di = Integer.MIN_VALUE;
        assertEquals(i, di);
    }

    @Test
    void setMaxSizeOfType() {
        String type = "float";
        double expected = Float.MAX_VALUE;
        assertEquals(expected,Level2.setMaxSizeOfType(type));
        type = "short";
        expected = Short.MAX_VALUE;
        assertEquals(expected,Level2.setMaxSizeOfType(type));
        type = "double";
        expected = Double.MAX_VALUE;
        assertEquals(expected,Level2.setMaxSizeOfType(type));
        type = "long";
        expected = Long.MAX_VALUE;
        assertEquals(expected,Level2.setMaxSizeOfType(type));
        long l = Long.MAX_VALUE;
        double dl = (double) Long.MAX_VALUE;
        assertEquals(l,dl);
        int i = Integer.MAX_VALUE;
        double di = Integer.MAX_VALUE;
        assertEquals(i, di);
    }

    @Test
    void haveDotsOnIntegerValue() {
        String type = "double";
        double min = 1.5;
        double max = 7.8;
        double increment = 8.9;
        assertTrue(Level2.haveDotsOnIntegerValue(type,min,max,increment));
        type = "byte";
        min = 1;
        max = 7;
        increment = 8;
        assertTrue(Level2.haveDotsOnIntegerValue(type,min,max,increment));
        type = "long";
        min = 1232;
        max = 743434.4;
        increment = 8342424;
        assertFalse(Level2.haveDotsOnIntegerValue(type,min,max,increment));
    }
}