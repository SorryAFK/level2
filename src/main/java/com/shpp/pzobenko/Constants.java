package com.shpp.pzobenko;

final class Constants {
    private Constants() {
        throw new IllegalStateException("Utility class");
    }
    public static final String TYPE_OF_VALUES_NAME = "TYPE_OF_VALUES";
    public static final String NAME_OF_PROP_FILE = "prop.properties";
    public static final String MESSAGE_FOR_MAX = "The max value has been defined as for ";
    public static final String MESSAGE_FOR_MIN = "The min value has been defined as for ";
    public static final String MESSAGE_TO_NOT_FLOAT_POINT_TYPE = "Your value have float point context on not float point type: ";
    public static final String YOUR_VALUE_MESSAGE = ", your value ";
    public static final String VALUE_FOR_REPLACE_INTEGER_NUMBERS = ".0";
    public static final String DOUBLE = "double";
    public static final String FLOAT = "float";
    public static final String BYTE = "byte";
    public static final String SHORT = "short";
    public static final String INT = "int";
    public static final String LONG = "long";
}
