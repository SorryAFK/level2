package com.shpp.pzobenko;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class Level2 {
    static Logger log = LoggerFactory.getLogger(Level2.class);

    public static void main(String[] args) {
        loadPropertiesNCallMultiplicationTable();
    }

    private static void loadPropertiesNCallMultiplicationTable() {
        log.info("\"loadProperties()\" start working.");
        String typeFromSystem = defineTheType(Constants.TYPE_OF_VALUES_NAME);
        if (findTypeFromProp(typeFromSystem)) {
            log.info("type from user is ");
            log.info(typeFromSystem);
            log.info("\n");
            Properties properties = new Properties();
            try {
                properties.load(Level2.class.getClassLoader().getResourceAsStream(Constants.NAME_OF_PROP_FILE));
            } catch (IOException e) {
                log.error("File not found", e);
            }
            double min = Double.parseDouble(properties.getProperty("MIN"));
            double max = Double.parseDouble(properties.getProperty("MAX"));
            double increment = Double.parseDouble(properties.getProperty("INCREMENT"));
            messageToLog(min,max,increment);
            String result = multiplicationTable(min, max, increment, typeFromSystem);
            log.info(result);
        } else {
            log.error("Boom", new TypeNotSupportedException(typeFromSystem));
        }
    }

    private static void messageToLog(double min, double max, double increment) {
        String message = String.valueOf(min);
        log.info("Values from prop file: min = ");
        log.info(message);
        log.info(", max = ");
        message = String.valueOf(max);
        log.info(message);
        log.info(", increment = ");
        message = String.valueOf(increment);
        log.info(message);
        log.info("." + "\n");
    }

    public static String defineTheType(String typeOfValues) {
        String typeFromSystem = System.getProperty(typeOfValues);
        if (typeFromSystem != null) {
            log.info("Type is not null" + "\n");
            return typeFromSystem.toLowerCase();
        } else {
            log.info("Type is null so define type with default value \"int\"" + "\n");
            return Constants.INT;
        }
    }

    public static boolean findTypeFromProp(String typeFromSystem) {
        String[] numbersTypes = {Constants.DOUBLE, Constants.FLOAT, Constants.BYTE, Constants.SHORT, Constants.INT, Constants.LONG};
        for (String numbersType : numbersTypes) {
            if (typeFromSystem.toLowerCase().equals(numbersType)) {
                log.info("The type can be processed by our code" + "\n");
                return true;
            }
        }
        log.info("The type can`t be processed by our code" + "\n");
        return false;
    }

    public static String multiplicationTable(double min, double max, double increment, String type) {
        double result;
        double minSize = setMinSizeOfType(type);
        double maxSize = setMaxSizeOfType(type);
        StringBuilder table = new StringBuilder();
        if (!haveDotsOnIntegerValue(type, min, max, increment)) {
            return ("You have wrong number format try to edit your values!" + "\n");
        }
        String message;
        for (double i = min; i < max + 1; i += increment) {
            for (double j = min; j < max + 1; j += increment) {
                result = (j * i);
                if (result > maxSize || result < minSize) {
                    message = j + "*" + i + "=* ";
                    log.info(message);
                    table.append("\"").append("*").append("\" ");
                } else {
                    message = j + "*" + i + "=" + result + " ";
                    log.info(message);
                    table.append("\"").append(result).append("\" ");
                }
            }
            log.info("\n");
            table.append("\n");
        }
        if (type.equals(Constants.FLOAT) || type.equals(Constants.DOUBLE)) {
            return table.toString();
        } else {
            return table.toString().replace(Constants.VALUE_FOR_REPLACE_INTEGER_NUMBERS, "");
        }
    }

    public static double setMinSizeOfType(String type) {
        switch (type) {
            case Constants.DOUBLE:
                log.info(Constants.MESSAGE_FOR_MIN + Constants.DOUBLE);
                return Double.MIN_VALUE;
            case Constants.FLOAT:
                log.info(Constants.MESSAGE_FOR_MIN + Constants.FLOAT);
                return Float.MIN_VALUE;
            case Constants.BYTE:
                log.info(Constants.MESSAGE_FOR_MIN + Constants.BYTE);
                return Byte.MIN_VALUE;
            case Constants.SHORT:
                log.info(Constants.MESSAGE_FOR_MIN + Constants.SHORT);
                return Short.MIN_VALUE;
            case Constants.LONG:
                log.info(Constants.MESSAGE_FOR_MIN + Constants.LONG);
                return Long.MIN_VALUE;
            default:
                log.info(Constants.MESSAGE_FOR_MIN + Constants.INT);
                return Integer.MIN_VALUE;
        }
    }

    public static double setMaxSizeOfType(String type) {
        switch (type) {
            case Constants.DOUBLE:
                log.info(Constants.MESSAGE_FOR_MAX + Constants.DOUBLE);
                return Double.MAX_VALUE;
            case Constants.FLOAT:
                log.info(Constants.MESSAGE_FOR_MAX + Constants.FLOAT);
                return Float.MAX_VALUE;
            case Constants.BYTE:
                log.info(Constants.MESSAGE_FOR_MAX + Constants.BYTE);
                return Byte.MAX_VALUE;
            case Constants.SHORT:
                log.info(Constants.MESSAGE_FOR_MAX + Constants.SHORT);
                return Short.MAX_VALUE;
            case Constants.LONG:
                log.info(Constants.MESSAGE_FOR_MAX + Constants.LONG);
                return Long.MAX_VALUE;
            default:
                log.info(Constants.MESSAGE_FOR_MAX + Constants.INT);
                return Integer.MAX_VALUE;
        }
    }

    public static boolean haveDotsOnIntegerValue(String type, double min, double max, double increment) {
        String message;
        if (type.equals(Constants.FLOAT) || type.equals(Constants.DOUBLE)) {
            log.info("Our value is not Integer we can process this type with this values" + "\n");
            return true;
        }
        String valueOfMin = String.valueOf(min);
        if (findNumbersAfterDotOnValues(valueOfMin)) {
            message = Constants.MESSAGE_TO_NOT_FLOAT_POINT_TYPE + type + Constants.YOUR_VALUE_MESSAGE + min + "\n" ;
            log.info(message);
            return false;
        }
        String valueOfMax = String.valueOf(max);
        if (findNumbersAfterDotOnValues(valueOfMax)) {
            message = Constants.MESSAGE_TO_NOT_FLOAT_POINT_TYPE + type + Constants.YOUR_VALUE_MESSAGE + max + "\n";
            log.info(message);
            return false;
        }
        String valueOfIncrement = String.valueOf(increment);
        if (findNumbersAfterDotOnValues(valueOfIncrement)) {
            message = Constants.MESSAGE_TO_NOT_FLOAT_POINT_TYPE + type + Constants.YOUR_VALUE_MESSAGE + increment + "\n" ;
            log.info(message);
            return false;
        }
        return true;
    }

    private static boolean findNumbersAfterDotOnValues(String valueOf) {
        for (int i = valueOf.indexOf('.'); i < valueOf.length(); i++) {
            if (Character.isDigit(valueOf.charAt(i)) && valueOf.charAt(i) != '0') {
                return true;
            }
        }
        return false;
    }
}