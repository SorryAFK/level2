package com.shpp.pzobenko;

public class TypeNotSupportedException extends Exception {
    public TypeNotSupportedException(String message) {
        super("The specified type is not supported by the program, please read the Terms of Reference and try again." +
                "\n" + "We remind you entered " + message + ".");
    }
}